# Toy Robot Simulator

[ ![Codeship Status for yuritomanek/toy-robot-simulation](https://app.codeship.com/projects/d97211c0-f7a2-0135-594b-7af7195f2991/status?branch=master)](https://app.codeship.com/projects/277236)
[![Maintainability](https://api.codeclimate.com/v1/badges/eefecffe8f95bd3f1703/maintainability)](https://codeclimate.com/github/yuritomanek/toy-robot-simulation/maintainability)

### Execution

To run the simulator, execute:

    ruby ./simulation.rb [test-data.txt]

### Example Input and Output:

#### Example 1

    PLACE 0,0,NORTH
    MOVE
    REPORT

Expected output:

    0,1,NORTH

#### Example 2

    PLACE 0,0,NORTH
    LEFT
    REPORT

Expected output:

    0,0,WEST

#### Example 3

    PLACE 1,2,EAST
    MOVE
    MOVE
    LEFT
    MOVE
    REPORT

Expected output

    3,3,NORTH
