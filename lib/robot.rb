class Robot

  DIRECTIONS = [:north, :east, :south, :west]

  def initialize
    @tabletop = Tabletop.new
    @x, @y, @direction = nil, nil, nil
    @placed = false
  end

  # Check if the bot has been placed on the table.
  def placed?
    return @placed
  end

  # Place the bot on the table at x and y, facing the direction supplied.
  def place(x, y, direction)
    x = x.to_i
    y = y.to_i
    if valid_position?(x, y)
      @x = x
      @y = y
      @direction = direction.downcase.to_sym
      @placed = true
      return true
    else
      return false
    end
  end

  # Check the new position is valid
  def valid_position?(x, y)
    x <= @tabletop.height &&
    y <= @tabletop.width &&
    x >= 0 &&
    y >= 0
  end

  # Report the position.
  def report
    puts "#{@x}, #{@y}, #{@direction.to_s.upcase}" if @placed
  end

  # Turn right 90 degrees.
  def right
    if @placed
      @direction = DIRECTIONS[DIRECTIONS.index(@direction) + 1] || DIRECTIONS[0]
    end
  end

  # Turn left 90 degrees.
  def left
    @direction = DIRECTIONS[DIRECTIONS.index(@direction) - 1] if @placed
  end

  # Check the direction and call the required move method
  def move
    if @placed
      case @direction
      when :north
        move_north
      when :east
        move_east
      when :south
        move_south
      when :west
        move_west
      end
    end
  end

  # Check the direction and call the required move method to jump 2 spaces
  def jump
    if @placed
      case @direction
      when :north
        move_north(2)
      when :east
        move_east(2)
      when :south
        move_south(2)
      when :west
        move_west(2)
      end
    end
  end

  # Move north a unit based on passed unit
  # Check move is allowed
  def move_north(spaces = 1)
    if @placed
      @y += spaces if @y < @tabletop.height
    end
  end

  # Move south a unit based on passed unit
  # Check move is allowed
  def move_south(spaces = 1)
    if @placed
      @y -= spaces if @y > 0
    end
  end

  # Move east a unit based on passed unit
  # Check move is allowed
  def move_east(spaces = 1)
    if @placed
      @x += spaces if @x < @tabletop.width
    end
  end

  # Move west a unit based on passed unit
  # Check move is allowed
  def move_west(spaces = 1)
    if @placed
      @x -= spaces if @x > 0
    end
  end

end
