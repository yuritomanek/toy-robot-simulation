class Tabletop
  attr_accessor :width, :height

  # Set the default width/height as 4 as the grid starts at 0
  DEFAULT_WIDTH = 4
  DEFAULT_HEIGHT = 4

  def initialize
    @width = DEFAULT_WIDTH
    @height = DEFAULT_HEIGHT
  end

end
