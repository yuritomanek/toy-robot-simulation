load './lib/tabletop.rb'
load './lib/robot.rb'

class Simulation

  COMMANDS = [:place, :report, :move, :right, :left, :quit]

  class << self

    # Start the simulation.
    # Create a bot and wait for input via a file or command.
    def start
      @bot = Robot.new
      ARGF.each_line { |line|
        command, args = parse(line)
        if command && COMMANDS.include?(command) && command == :quit
          break
        else
          run(command, args)
        end
      }
    end

    # Parse the input and split into a command and argument if supplied.
    def parse(input)
      input = input.chomp
      args = input.split
      command = args.shift
      command = command.downcase.to_sym if command
      args = args.join.split(",")
      return command, args
    end

    # Run the command against the bot.
    def run(command, args)
      if command && COMMANDS.include?(command)
        if args.any?
          @bot.send(command, *args)
        else
          @bot.send(command)
        end
      end
    end

  end

end

Simulation.start
