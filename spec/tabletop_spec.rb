require 'spec_helper'
require 'tabletop'

describe Tabletop do

  let(:tabletop) { Tabletop.new }

  it 'returns Tabletop as the object' do
    expect(tabletop).to be_a(Tabletop)
  end

  it 'returns tabletop height of DEFAULT_HEIGHT' do
    expect(tabletop.height).to eql(Tabletop::DEFAULT_HEIGHT)
  end

  it 'returns tabletop width of DEFAULT_WIDTH' do
    expect(tabletop.width).to eql(Tabletop::DEFAULT_WIDTH)
  end

end
