require 'spec_helper'
require 'tabletop'
require 'robot'

describe Robot do

  let(:robot) { Robot.new }

  describe "#new" do
    it 'returns Robot as the object' do
      expect(robot).to be_a(Robot)
    end
  end

  describe "#place" do
    it "returns a valid place by returning true" do
      expect(robot.place(3,2,:south)).to eql(true)
    end

    it "returns a invalid place by returning false" do
      expect(robot.place(7,-1,:south)).to eql(false)
    end
  end

  describe "#placed?" do
    it "returns true after a valid place command" do
      robot.place(3,2,:south)
      expect(robot.placed?).to eql(true)
    end

    it "returns false when no place command has been executed." do
      expect(robot.placed?).to eql(false)
    end
  end

  describe "#report" do
    it "expects a response from report" do
      robot.place(3,2,:south)
      expect {robot.report}.to output("3, 2, SOUTH\n").to_stdout
    end

    it "expects a NO response from report" do
      robot.place(7,-1,:south)
      expect {robot.report}.to_not output.to_stdout
    end
  end

  describe "#left #right" do
    it "should turn left" do
      robot.place(0,0,:north)
      robot.left
      expect {robot.report}.to output("0, 0, WEST\n").to_stdout
    end

    it "should turn right" do
      robot.place(0,0,:north)
      robot.right
      expect {robot.report}.to output("0, 0, EAST\n").to_stdout
    end
  end

  describe "#move" do
    it "should move north one unit" do
      robot.place(0,0,:north)
      robot.move
      expect {robot.report}.to output("0, 1, NORTH\n").to_stdout
    end

    it "should not move north off the table" do
      robot.place(0,4,:north)
      robot.move
      expect {robot.report}.to output("0, 4, NORTH\n").to_stdout
    end

    it "should move south one unit" do
      robot.place(0,1,:south)
      robot.move
      expect {robot.report}.to output("0, 0, SOUTH\n").to_stdout
    end

    it "should not move south off the table" do
      robot.place(0,0,:south)
      robot.move
      expect {robot.report}.to output("0, 0, SOUTH\n").to_stdout
    end

    it "should move east one unit" do
      robot.place(0,0,:east)
      robot.move
      expect {robot.report}.to output("1, 0, EAST\n").to_stdout
    end

    it "should not move east off the table" do
      robot.place(4,0,:east)
      robot.move
      expect {robot.report}.to output("4, 0, EAST\n").to_stdout
    end

    it "should move west one unit" do
      robot.place(4,0,:west)
      robot.move
      expect {robot.report}.to output("3, 0, WEST\n").to_stdout
    end

    it "should not move west off the table" do
      robot.place(0,0,:west)
      robot.move
      expect {robot.report}.to output("0, 0, WEST\n").to_stdout
    end
  end

  describe "#jump" do
    it "should jump north two units" do
      robot.place(0,0,:north)
      robot.jump
      expect {robot.report}.to output("0, 2, NORTH\n").to_stdout
    end

    it "should not jump north off the table" do
      robot.place(0,4,:north)
      robot.jump
      expect {robot.report}.to output("0, 4, NORTH\n").to_stdout
    end

    it "should jump south two units" do
      robot.place(0,2,:south)
      robot.jump
      expect {robot.report}.to output("0, 0, SOUTH\n").to_stdout
    end

    it "should not jump south off the table" do
      robot.place(0,0,:south)
      robot.jump
      expect {robot.report}.to output("0, 0, SOUTH\n").to_stdout
    end

    it "should jump east two units" do
      robot.place(0,0,:east)
      robot.jump
      expect {robot.report}.to output("2, 0, EAST\n").to_stdout
    end

    it "should not jump east off the table" do
      robot.place(4,0,:east)
      robot.jump
      expect {robot.report}.to output("4, 0, EAST\n").to_stdout
    end

    it "should jump west two units" do
      robot.place(4,0,:west)
      robot.jump
      expect {robot.report}.to output("2, 0, WEST\n").to_stdout
    end

    it "should not jump west off the table" do
      robot.place(0,0,:west)
      robot.jump
      expect {robot.report}.to output("0, 0, WEST\n").to_stdout
    end
  end

end
